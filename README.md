# README #

Contains large files (programs, presentations, images) from PETSc annual meetings.
The high level pages (.rst files) that point point to this material are in the doc/community/meetings directory of the PETSc repository.

The files are all in the public directory so that the .gitlab-ci.yml file automatically displays all the files at https://petsc.gitlab.io/annual-meetings
```
